//alert("test");

/*
Bulk AJAX/AHAH stock updater module for Drupal 
Copyright (C) 2010 Antonio Molinari (www.incode.it)
Based on module by Ramesh Nair (www.hiddentao.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

var g_attr_orig = "orig";
var search_enabled = true;
var mis_chars_for_search = 3;
var xhr;

$(document).ready(function(){
	uc_custom_stock_updater_init();
});

function uc_custom_stock_updater_init() {
	//alert("init");
	$('.uc_custom_stock_updater-value').keypress(function(event){
		//alert(event.keyCode);
	    if(event.keyCode == 13) {
			if (!search_enabled) {
				event.preventDefault();
	      		return search_enabled;
			}
	    }
	 });
	
	$("#edit-uc-custom-stock-updater-filter").focus();
	$("#edit-uc-custom-stock-updater-filter").click(function(){
		//this.select();
		uc_custom_stock_updater_select_textbox(this);
		search_enabled = true;
		//$(this).val("");
	});
	
	$("#edit-search-button").click(function(event){
		/*event.preventDefault();
		event.stopPropagation();
		$("#edit-uc-custom-stock-updater-filter").trigger("change");
		*/
		if (!search_enabled) {
			event.preventDefault();
			event.stopPropagation();
		}
	});

	$(document).ajaxSuccess(function (e, xhr, settings){
		//alert(e+" "+xhr+" "+settings);
		if (settings.url.indexOf("_uc_custom_stock_updater_MENU_RESULTS")>-1)
			uc_custom_stock_updater_update_list();
	});
	
	//$("#edit-uc-custom-stock-updater-filter").keyup);
	$("#edit-uc-custom-stock-updater-filter").unbind("keyup").keyup(function (event){
		/*
		var tf = $(this);
		var str = $(this).val();
		if (str.length >= mis_chars_for_search) {
			tf.trigger("change");
		}
		*/
	});
}

function uc_custom_stock_updater_select_textbox(textbox)
{
    setTimeout(function() { textbox.select(); }, 10);
}

function uc_custom_stock_updater_update_list() {

	//alert("updating list!");
	// keep track of which stock levels get edited
	$(".ahah-progress-throbber").hide();
	$("input.uc_custom_stock_updater-value").each(function(){
		$(this).attr(g_attr_orig, $(this).val());
		$(this).focus(function(){
			//this.select();
			uc_custom_stock_updater_select_textbox(this);
		});
		/*
		$(this).change(function(){
			if (confirm(Drupal.t('Confirm stock value update?'))) {
				uc_custom_stock_updater_submit_value(this);
			} else {
				$(this).val($(this).attr(g_attr_orig))
			}
		})
		*/
	});
	
	search_enabled = false;
	var rows = $("#uc_custom_stock .uc_custom_stock_updater-value");
	rows.each(function(element,index) {
		var element = $(this);
		element.keypress(function(event) {
			if (event.keyCode == '13') {
				event.preventDefault();
				if (confirm("Confirm update of stock from to "+element.val()+"?")) {
					uc_custom_stock_updater_submit_value(element);
				} else {
					$(element).val($(element).attr(g_attr_orig));
				}
			}
			/*
			   xTriggered++;
			   var msg = 'Handler for .keypress() called ' + xTriggered + ' time(s).';
			  $.print(msg, 'html');
			  $.print(event);
			*/
		});
	});
	
	// check if one result has been returned
	if(rows.length>0) {
		var first_value = $(rows[0]);
		$(rows[0]).focus();
		//$(rows[0]).select();
		uc_custom_stock_updater_select_textbox(rows[0]);
		if(rows.length==1) {
			//first_value.select();
			uc_custom_stock_updater_select_textbox(first_value);
			var old_val = parseInt(first_value.val());
			var new_val = old_val-1;
			if (old_val>0) {
				if (confirm("Confirm descrease of stock from "+old_val+" to "+new_val+"?")) {
					first_value.val(new_val);
					uc_custom_stock_updater_submit_value(first_value);
				} else {
					//$("#list-container").empty();
				}
			}
		}
	}
	//uc_custom_stock_updater_init();
	
	// hide empty images
	/*
	$("table#uc_custom_stock img").each(function(index,element){
		var img = $(this);
		var has_image = img.attr("src").toLowerCase().index(".jpg");
		img.hide();
	});
	*/

}


function uc_custom_stock_updater_submit_value(stockInputElem)
{
	var _sku = $(stockInputElem).attr("name");
	var _stock = $(stockInputElem).val();
	//alert("sku:"+_sku+" stock:"+_stock+" url:"+g_uc_custom_stock_updater_ajax_url);

	if ( parseInt(_stock) >= 0) {
		// reset
		$(stockInputElem)
			.removeClass("error")
			.after("<div class=\"uc_custom_stock_updater_ajax_progress\"></div>");

		$(stockInputElem).nextAll("div.uc_custom_stock_updater_ajax_error").remove();

		// call
		/*
	    if(xhr && xhr.readystate != 4){
            xhr.abort();
        }
		*/
		xhr = $.ajax({
			url : g_uc_custom_stock_updater_ajax_url,
			type: 'POST',
			timeout : 10000,
			data : { sku: _sku, stock: _stock },
			dataType : "json",
		    error : function(_XMLHttpRequest, _textStatus, _errorThrown)
		    {
				uc_custom_stock_updater_ajax_error(stockInputElem, _textStatus);
				//alert("error");
		    },			
			success : function(_data, _textStatus, _XMLHttpRequest)
			{
				//alert("success");
		    	if (undefined != _data.error)
						uc_custom_stock_updater_ajax_error(stockInputElem, _data.error);
		    	else {
		    		$(stockInputElem).attr(g_attr_orig, _stock);
						//alert("ok");
						$("#edit-uc-custom-stock-updater-filter").val("");
						$("#edit-uc-custom-stock-updater-filter").focus();
						//$("#edit-uc-custom-stock-updater-filter").select();
						uc_custom_stock_updater_select_textbox($("#edit-uc-custom-stock-updater-filter"));
						search_enabled = true;
						$("#list-container").empty();
					}
		  },
			complete : function(_XMLHttpRequest,_textStatus)
			{
				//alert("complete");
				$(stockInputElem).nextAll("div.uc_custom_stock_updater_ajax_progress").remove();
			}	    
		});
	} else {
		alert("Stock level must be greater than or equal to 0!");
		$(stockInputElem).val($(stockInputElem).attr(g_attr_orig));
	}
}


function uc_custom_stock_updater_ajax_error(stockInputElem, _errorMsg)
{
	$(stockInputElem)
		.after("<div class=\"uc_custom_stock_updater_ajax_error\">" + _errorMsg + "</div>")
		.val($(stockInputElem).attr(g_attr_orig))
		.addClass("error");
}


