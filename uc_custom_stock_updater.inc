<?php
// $Id$
/*
Bulk AJAX/AHAH stock updater module for Drupal 
Copyright (C) 2010 Antonio Molinari (www.incode.it)
Based on module by Ramesh Nair (www.hiddentao.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


/**
 * Main admin page.
 * @return HTML.
 */
function uc_custom_stock_updater_overview() {
	drupal_goto(_uc_custom_stock_updater_MENU_LIST);
	return;
}

/**
 * Stock levels editing page. AHAH version
 * @return HTML.
 */
function uc_custom_stock_updater_list() {
	drupal_add_css(drupal_get_path('module', 'uc_stock') . '/uc_stock.css');
  drupal_add_css(drupal_get_path('module', 'uc_custom_stock_updater') . '/uc_custom_stock_updater.css');
  drupal_add_js('var g_uc_custom_stock_updater_ajax_url = "'.url(_uc_custom_stock_updater_MENU_UPDATE_VALUE).'";', 'inline');
  drupal_add_js(drupal_get_path('module', 'uc_custom_stock_updater') . '/uc_custom_stock_updater.js');
	
	$form['uc_custom_stock_updater_filter'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Filter by SKU'),
    //'#description'   => t('Insert a Sku or a part of it. Optionally poit the barcode reader to get the full and correct code.'),
    '#weight'        => -4,
    '#attributes' => array('autocomplete' => 'off'),
    #'#ahah'          => array(
		#	'method' => 'replace',
		#	'event' => 'change',
    #  'path'    => _uc_custom_stock_updater_MENU_RESULTS,
		#  'wrapper' => 'list-container',
    #),
  );

	$form['search_button'] = array(
		'#type' => 'button',
		'#value' => t('Search'),
		'#weight' => -3,
		#'#executes_submit_callback' => FALSE,
		//'#submit' => array('mymodule_form_cancel'),
		'#ahah'          => array(
			'method' => 'replace',
			'event' => 'click',
      'path'    => _uc_custom_stock_updater_MENU_RESULTS,
		  'wrapper' => 'list-container',
    ),
	);

	$form['list_container'] = array(
		'#weight' => -2,
	  '#value' => '<div id="list-container"></div>',
	);
	
	return $form;
}

/**
* AJAX Callback function.
*/
function uc_custom_stock_updater_results() {
 
  $filter = $_POST['uc_custom_stock_updater_filter'];
 	
 	$page_size = variable_get('uc_reports_table_size', 30);
  $rows = array();

  $header = array(
	array('data' => t('Image'), 'field' => 'image'),
    array('data' => t('SKU'), 'field' => 'sku'),
    array('data' => t('Product'), 'field' => 'title'),
    array('data' => t('Stock'), 'field' => 'stock'),
    array('data' => t('Threshold'), 'field' => 'threshold'),
		array('data' => ""),
  );
	
  /* OLD MODE */
	$sql_from_and_where_conditions = ' FROM {uc_product_stock} as s 
	LEFT JOIN {node} as n ON s.nid = n.nid 
	WHERE s.sku LIKE "%'.$filter.'%" ';
	
	
   /*  NEW MODE 
	$sql_from_and_where_conditions = ' FROM {uc_product_stock} as s 
	LEFT JOIN {node} as n ON s.nid = n.nid LEFT JOIN uc_products p ON s.sku = p.model 
	WHERE p.model LIKE "%'.$filter.'%" ';
   */
   
   // DEBUG, DO NOT TOUCH
	//WHERE title <> "" AND s.sku LIKE "%'.$filter.'%" ';
	//WHERE active = 1 AND title <> "" AND s.sku LIKE "%'.$filter.'%" ';
	
	$sql_count = 'SELECT count(s.nid) '.$sql_from_and_where_conditions;
	$sql_search = 'SELECT s.nid, sku, title, stock, threshold '.$sql_from_and_where_conditions;
	
  #$result = pager_query($sql . tablesort_sql($header), $page_size, 0, NULL);
	$max_num_rows = 100;
	$num_rows = (int)db_result(db_query($sql_count));

  if ($num_rows <= $max_num_rows) {
		$result = db_query($sql_search);
		$output = '';
		$count = 0;
	  while ($stock = db_fetch_object($result)) {
			$count += 1;
		 	$stock->sku = check_plain($stock->sku);
		 	$stock->title = check_plain($stock->title);
	
		 	// for filtering purposes
		 	$filter_val = strtolower($stock->sku . $stock->title . $stock->stock . $stock->threshold .$stock->field_barcode_value);
		   $node = node_load($stock->nid);
		   // Add the data to a table row for display.
		
		   $image_path =  $node->field_gallery[0]['filepath'];
		   $imagecache_image_tag = theme('imagecache','product_thumb', $image_path , "alt", "title", "attributes");
		   $rows[] = array(
		     'data' => array(
		       //array('data' => theme('imagecache','custom_stock_updater', $image_path , "alt", "title", "attributes")),
			   array('data' => $imagecache_image_tag),
			   array('data' => "<span id=\"$filter_val\">$stock->sku</span>"),
		       array('data' => l($stock->title, 'node/' . $stock->nid)),
		       array('data' => "<input tabindex=\"".$count."\" size=\"3\" maxlength=\"9\" class=\"uc_custom_stock_updater-value\" id=\"nid-".$stock->nid."-stock\" name=\"".$stock->sku."\" value=\"$stock->stock\" />"),
		       array('data' => $stock->threshold),
				array('data' => "<a href=\"".base_path().'node/'.$stock->nid.'/edit/stock?destination='._uc_custom_stock_updater_MENU_LIST."\">".t("stock detail")."</a>")
		     ),
		     'class' => (intval($stock->threshold) >= intval($stock->stock)) ? 'uc-stock-below-threshold' : 'uc-stock-above-threshold',
		   );
		}

		if ($count >0 ) {
			$output .= theme('table', $header, $rows, array('width' => '100%', 'class' => 'uc-stock-table', 'id' => 'uc_custom_stock')). theme_pager(NULL, $page_size);
			$output .= '<script type="text/javascript">uc_custom_stock_updater_update_list();</script>';
		}
		else {
			$output .= "<div>".t("No products found with that SKU or part of it.", array('!count' => $count))."</div><br/>";
			$output .= '<script type="text/javascript">uc_custom_stock_updater_select_textbox($("#edit-uc-custom-stock-updater-filter"));</script>';
			
		}
	
	  #$output .= '<pre>'.$sql_search.'</pre>';
		$output .= '<div class="uc-reports-links">' . l(t('Reload page'), _uc_custom_stock_updater_MENU_UPDATE) . '</div>';  
		#$output = $sql;
	  #$output = drupal_render($form, 'list-container');
	} else {
		$output = '<script type="text/javascript">alert("Too many results ('.$num_rows.' with limit at '.$max_num_rows.'), please try to be more specific...");</script>';	
	}
  print drupal_to_js(array('data' => $output, 'status' => true));
  exit();
}



/** 
 * AJAX callback.
 */
function uc_custom_stock_updater_update_value() {
	if (!isset($_POST['sku']) || !isset($_POST['stock']) || 0 > intval($_POST['stock'])) {
		$err = t('Invalid parameters supplied');
	} else {
		$from_stock = uc_stock_level($_POST['sku']);
		$query_result = db_query("UPDATE {uc_product_stock} SET stock = %d WHERE sku = '%s'", $_POST['stock'], $_POST['sku']);
  		if ($query_result) {
  			watchdog('custom_stock_update', '!sku from !from_stock to !to_stock', $variables = array('!sku' => $_POST['sku'], '!from_stock' => $from_stock,'!to_stock' => $_POST['stock']));
  		} else {
  			$err = t('Error saving to db');
  		}
			
	}
	
	$ret = array();
	
	if (isset($err))
		$ret['error'] = $err;
		
	drupal_json($ret);
	exit();
}



/** 
 * function to get the TABLE or COLUMN for a CCK FIELD 
 * method adapted from http://drewish.com/content/2010/06/correctly_accessing_cck_fields_in_sql_queries 
 * 
 * @param $field_name 
 * @param $type 'table' or 'column' 
 * 
 * to get both table and column, run function twice with each $type 
 * Read more: http://benbuckman.net/tech/10/08/drupal-using-cck-fieldss-inconsistent-tables-custom-queries#ixzz0wL0MLNex
*/ 

function helper_cck_field_sql($field_name, $type) { 
	$field = content_fields($field_name); 
	$db_info = content_database_info($field);   
	if ($type == 'table' && isset($db_info['table'])) { 
		return $db_info['table']; 
	} elseif ($type == 'column' && isset($db_info['columns']['value']['column'])) { 
		return $db_info['columns']['value']['column']; 
	} 
	return NULL; 
}





